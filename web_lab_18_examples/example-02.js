
// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
// ------------------------------------------------------------------------------


// Playing around with custom modules
// ------------------------------------------------------------------------------
var routes = require('./example-02-routes.js');
routes(app);

var calc = require('./example-02-calc.js');
console.log(calc.x);
console.log(calc.addX(1));

var User = require('./example-02-user.js');
var tom = new User(1, 'Thomas the T-Rex');
console.log(tom.userInfo());
// ------------------------------------------------------------------------------


// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});