
// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// Allow us to use SQLite3 from node.js
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('example-01.db');
// ------------------------------------------------------------------------------


// Route handlers
// ------------------------------------------------------------------------------

// When the user navigates to "/", load some articles from an SQLite database and display their summary to the user.
app.get(['/', '/articles'], function (req, res) {

    // NOTE: "substr(content, 1, 100)" will select the first 100 characters of the "content" column (1 instead of 0 because SQL is 1-based instead of 0-based).
    // NOTE 2: "||" is the string concatentation opteration in SQL.
    // So, in other words, the below SELECT statement will get the first 100 characters of content, followed by "..." (the user would then click to see more).

    // Will execute the given query, and then execute the given callback function with the result.
    db.all("SELECT id, title, substr(content, 1, 100) || '...' as 'content' FROM articles", function (err, rows) {

        // for (var i = 0; i < rows.length; i++) {
        //     console.log(rows[i]);
        // }

        // "rows" is a JavaScript array, and we can do anything we would normally do for such an array. Here, we just pass it through to a page to render.
        res.render("articles", { articles: rows });
    });

});

// When the user navigates to "/articles/:id", load the article with the given id and display it to the user.
app.get('/articles/:id', function (req, res) {

    // Read the id of the article to get from the request params (corresponds to :id above).
    var articleId = req.params.id;

    // Will execute the given query, and then execute the given callback function with the result.
    // We can identify query parameters with a "?", and supply them in an array, as shown here.
    db.all("SELECT title, content FROM articles WHERE id = ?", [articleId], function (err, rows) {

        var article = rows[0]; // There should only be one...

        res.render("article-detail", { title: article.title, content: article.content });
    });

});

// When the user POSTs to "/articles", add a new article to the database, then redirect back to articles display.
app.post('/articles', function (req, res) {

    var articleTitle = req.body.title;
    var articleContent = req.body.content;

    // db.run will execute the given non-SELECT SQL statemtne, with the given parameters, then call the given function
    // when complete.
    db.run("INSERT INTO articles (title, content) VALUES (?, ?)", [articleTitle, articleContent], function (err) {

        // Within this function, you can use this.lastID to get the automatically-generated ID that was just inserted into the db.
        console.log("New article added with id = " + this.lastID);

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        console.log(this.changes + " row(s) affected.");

        res.redirect('/articles');
    });
});

// When the user POSTs to "/delete/:id", delete the article with the given id, then redirect to /articles.
app.post('/delete/:id', function (req, res) {

    var articleId = req.params.id;

    db.run("DELETE FROM articles WHERE id = ?", [articleId], function (err) {

        // Within this function, you can use this.changes to get the number of rows affected by the query.
        console.log(this.changes + " row(s) affected.");

        res.redirect('/articles');
    });
});

// Allow the server to serve up files from the "public" folder.
app.use(express.static(__dirname + "/public"));
// ------------------------------------------------------------------------------


// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});